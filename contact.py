from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QMainWindow
from PyQt5.uic import loadUiType

ui, _ = loadUiType("./resource/ui/contact.ui")


# 联系与反馈信息界面
class contactDialog(QMainWindow, ui):
    def __init__(self):
        QMainWindow.__init__(self)
        self.setupUi(self)
        self.setWindowIcon(QIcon("./resource/icon/about.png"))
        self.setWindowTitle("联系反馈")
        self.setFixedSize(self.width(), self.height())
        self.label.setOpenExternalLinks(True)
        self.label.setTextInteractionFlags(Qt.TextBrowserInteraction)
