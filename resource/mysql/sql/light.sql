/*
 Navicat Premium Data Transfer

 Source Server         : traffic
 Source Server Type    : MySQL
 Source Server Version : 80023
 Source Host           : localhost:3306
 Source Schema         : light

 Target Server Type    : MySQL
 Target Server Version : 80023
 File Encoding         : 65001

 Date: 15/03/2021 09:35:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for light
-- ----------------------------
DROP TABLE IF EXISTS `light`;
CREATE TABLE `light`  (
  `id` int NULL DEFAULT NULL,
  `nodes_id` int NULL DEFAULT NULL,
  `phase` int NULL DEFAULT NULL,
  `r` int NULL DEFAULT NULL,
  `g` int NULL DEFAULT NULL,
  `y` int NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of light
-- ----------------------------
INSERT INTO `light` VALUES (1, 1, 1, 5, 4, 3);
INSERT INTO `light` VALUES (2, 2, 1, 6, 7, 3);
INSERT INTO `light` VALUES (3, 3, 1, 5, 9, 3);
INSERT INTO `light` VALUES (4, 4, 1, 5, 4, 3);
INSERT INTO `light` VALUES (5, 5, 1, 6, 7, 3);
INSERT INTO `light` VALUES (6, 6, 1, 5, 9, 3);
INSERT INTO `light` VALUES (7, 7, 1, 5, 4, 3);
INSERT INTO `light` VALUES (8, 8, 1, 6, 7, 3);
INSERT INTO `light` VALUES (9, 9, 1, 5, 9, 3);
INSERT INTO `light` VALUES (10, 10, 1, 5, 4, 3);
INSERT INTO `light` VALUES (11, 11, 1, 6, 7, 3);
INSERT INTO `light` VALUES (12, 12, 1, 5, 4, 3);
INSERT INTO `light` VALUES (13, 13, 1, 6, 7, 3);
INSERT INTO `light` VALUES (14, 14, 1, 5, 9, 3);
INSERT INTO `light` VALUES (15, 15, 1, 5, 4, 3);
INSERT INTO `light` VALUES (16, 16, 1, 6, 7, 3);
INSERT INTO `light` VALUES (17, 1, 2, 5, 9, 3);
INSERT INTO `light` VALUES (18, 2, 2, 5, 4, 3);
INSERT INTO `light` VALUES (19, 3, 2, 6, 7, 3);
INSERT INTO `light` VALUES (20, 4, 2, 5, 9, 3);
INSERT INTO `light` VALUES (21, 5, 2, 5, 4, 3);
INSERT INTO `light` VALUES (22, 6, 2, 6, 7, 3);
INSERT INTO `light` VALUES (23, 7, 2, 5, 4, 3);
INSERT INTO `light` VALUES (24, 8, 2, 6, 7, 3);
INSERT INTO `light` VALUES (25, 9, 2, 5, 9, 3);
INSERT INTO `light` VALUES (26, 10, 2, 5, 4, 3);
INSERT INTO `light` VALUES (27, 11, 2, 6, 7, 3);
INSERT INTO `light` VALUES (28, 12, 2, 5, 9, 3);
INSERT INTO `light` VALUES (29, 13, 2, 5, 4, 3);
INSERT INTO `light` VALUES (30, 14, 2, 6, 7, 3);
INSERT INTO `light` VALUES (31, 15, 2, 5, 9, 3);
INSERT INTO `light` VALUES (32, 16, 2, 5, 4, 3);
INSERT INTO `light` VALUES (33, 1, 3, 6, 7, 3);
INSERT INTO `light` VALUES (34, 2, 3, 5, 4, 3);
INSERT INTO `light` VALUES (35, 3, 3, 6, 7, 3);
INSERT INTO `light` VALUES (36, 4, 3, 5, 9, 3);
INSERT INTO `light` VALUES (37, 5, 3, 5, 4, 3);
INSERT INTO `light` VALUES (38, 6, 3, 6, 7, 3);
INSERT INTO `light` VALUES (39, 7, 3, 5, 9, 3);
INSERT INTO `light` VALUES (40, 8, 3, 5, 4, 3);
INSERT INTO `light` VALUES (41, 9, 3, 6, 7, 3);
INSERT INTO `light` VALUES (42, 10, 3, 5, 9, 3);
INSERT INTO `light` VALUES (43, 11, 3, 5, 4, 3);
INSERT INTO `light` VALUES (44, 12, 3, 6, 7, 3);
INSERT INTO `light` VALUES (45, 13, 3, 5, 4, 3);
INSERT INTO `light` VALUES (46, 14, 3, 6, 7, 3);
INSERT INTO `light` VALUES (47, 15, 3, 5, 9, 3);
INSERT INTO `light` VALUES (48, 16, 3, 5, 4, 3);
INSERT INTO `light` VALUES (49, 1, 4, 6, 7, 3);
INSERT INTO `light` VALUES (50, 2, 4, 5, 9, 3);
INSERT INTO `light` VALUES (51, 3, 4, 5, 4, 3);
INSERT INTO `light` VALUES (52, 4, 4, 6, 7, 3);
INSERT INTO `light` VALUES (53, 5, 4, 5, 9, 3);
INSERT INTO `light` VALUES (54, 6, 4, 5, 4, 3);
INSERT INTO `light` VALUES (55, 8, 4, 6, 7, 3);
INSERT INTO `light` VALUES (56, 11, 4, 5, 4, 3);
INSERT INTO `light` VALUES (57, 12, 4, 6, 7, 3);
INSERT INTO `light` VALUES (58, 16, 4, 5, 9, 3);

SET FOREIGN_KEY_CHECKS = 1;
