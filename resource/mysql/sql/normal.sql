/*
 Navicat Premium Data Transfer

 Source Server         : traffic
 Source Server Type    : MySQL
 Source Server Version : 80023
 Source Host           : localhost:3306
 Source Schema         : normal

 Target Server Type    : MySQL
 Target Server Version : 80023
 File Encoding         : 65001

 Date: 15/03/2021 09:35:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for nodes
-- ----------------------------
DROP TABLE IF EXISTS `nodes`;
CREATE TABLE `nodes`  (
  `id` int NULL DEFAULT NULL,
  `node` int NULL DEFAULT NULL,
  `area` int NULL DEFAULT NULL,
  `cross` int NULL DEFAULT NULL,
  `max_phase` int NULL DEFAULT NULL,
  `x` int NULL DEFAULT NULL,
  `y` int NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of nodes
-- ----------------------------
INSERT INTO `nodes` VALUES (1, 1, 1, 1, 0, 14, 250);
INSERT INTO `nodes` VALUES (2, 1, 1, 2, 0, 146, 202);
INSERT INTO `nodes` VALUES (3, 1, 1, 3, 4, 268, 154);
INSERT INTO `nodes` VALUES (4, 1, 1, 4, 4, 369, 112);
INSERT INTO `nodes` VALUES (5, 1, 1, 5, 0, 550, 3);
INSERT INTO `nodes` VALUES (6, 1, 2, 6, 4, 38, 327);
INSERT INTO `nodes` VALUES (7, 1, 2, 7, 0, 168, 281);
INSERT INTO `nodes` VALUES (8, 1, 2, 8, 4, 293, 237);
INSERT INTO `nodes` VALUES (9, 1, 2, 9, 0, 399, 202);
INSERT INTO `nodes` VALUES (10, 1, 2, 10, 0, 522, 160);
INSERT INTO `nodes` VALUES (11, 1, 2, 11, 4, 606, 127);
INSERT INTO `nodes` VALUES (12, 1, 3, 12, 4, 83, 476);
INSERT INTO `nodes` VALUES (13, 1, 3, 13, 4, 329, 379);
INSERT INTO `nodes` VALUES (14, 1, 3, 14, 0, 432, 341);
INSERT INTO `nodes` VALUES (15, 1, 3, 15, 0, 553, 291);
INSERT INTO `nodes` VALUES (16, 1, 3, 16, 3, 643, 256);
INSERT INTO `nodes` VALUES (17, 0, 0, 0, 0, 6, 215);
INSERT INTO `nodes` VALUES (18, 0, 0, 0, 0, 248, 89);
INSERT INTO `nodes` VALUES (19, 0, 0, 0, 0, 345, 38);
INSERT INTO `nodes` VALUES (20, 0, 0, 0, 0, 3, 255);
INSERT INTO `nodes` VALUES (21, 0, 0, 0, 0, 3, 341);
INSERT INTO `nodes` VALUES (22, 0, 0, 0, 0, 641, 97);
INSERT INTO `nodes` VALUES (23, 0, 0, 0, 0, 3, 502);
INSERT INTO `nodes` VALUES (24, 0, 0, 0, 0, 98, 520);
INSERT INTO `nodes` VALUES (25, 0, 0, 0, 0, 488, 519);

-- ----------------------------
-- Table structure for roads
-- ----------------------------
DROP TABLE IF EXISTS `roads`;
CREATE TABLE `roads`  (
  `id` int NULL DEFAULT NULL,
  `area` int NULL DEFAULT NULL,
  `cross` int NULL DEFAULT NULL,
  `start_node` int NULL DEFAULT NULL,
  `dest_node` int NULL DEFAULT NULL,
  `start_x` int NULL DEFAULT NULL,
  `start_y` int NULL DEFAULT NULL,
  `dest_x` int NULL DEFAULT NULL,
  `dest_y` int NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roads
-- ----------------------------
INSERT INTO `roads` VALUES (1, 1, 1, 1, 2, 12, 245, 144, 197);
INSERT INTO `roads` VALUES (2, 1, 1, 2, 1, 148, 207, 16, 255);
INSERT INTO `roads` VALUES (3, 1, 2, 2, 3, 144, 197, 266, 149);
INSERT INTO `roads` VALUES (4, 1, 2, 3, 2, 270, 159, 148, 207);
INSERT INTO `roads` VALUES (5, 1, 3, 3, 4, 266, 149, 367, 107);
INSERT INTO `roads` VALUES (6, 1, 3, 4, 3, 371, 117, 270, 159);
INSERT INTO `roads` VALUES (7, 1, 5, 4, 5, 366, 108, 547, -1);
INSERT INTO `roads` VALUES (8, 1, 4, 5, 4, 553, 7, 372, 116);
INSERT INTO `roads` VALUES (9, 2, 6, 6, 7, 36, 322, 166, 276);
INSERT INTO `roads` VALUES (10, 2, 8, 7, 6, 170, 286, 40, 332);
INSERT INTO `roads` VALUES (11, 2, 7, 7, 8, 166, 276, 291, 232);
INSERT INTO `roads` VALUES (12, 2, 8, 8, 7, 295, 242, 170, 286);
INSERT INTO `roads` VALUES (13, 2, 8, 8, 9, 291, 232, 397, 197);
INSERT INTO `roads` VALUES (14, 2, 10, 9, 8, 401, 207, 295, 242);
INSERT INTO `roads` VALUES (15, 2, 9, 9, 10, 397, 197, 520, 155);
INSERT INTO `roads` VALUES (16, 2, 10, 10, 9, 524, 165, 401, 207);
INSERT INTO `roads` VALUES (17, 2, 9, 10, 11, 520, 155, 604, 122);
INSERT INTO `roads` VALUES (18, 2, 10, 11, 10, 608, 132, 524, 165);
INSERT INTO `roads` VALUES (19, 3, 13, 12, 13, 83, 476, 329, 379);
INSERT INTO `roads` VALUES (20, 3, 14, 13, 14, 329, 379, 432, 341);
INSERT INTO `roads` VALUES (21, 3, 15, 14, 15, 432, 341, 553, 291);
INSERT INTO `roads` VALUES (22, 3, 15, 15, 16, 551, 286, 641, 251);
INSERT INTO `roads` VALUES (23, 3, 16, 16, 15, 645, 261, 555, 296);
INSERT INTO `roads` VALUES (24, 2, 6, 1, 6, 9, 251, 33, 328);
INSERT INTO `roads` VALUES (25, 2, 6, 6, 1, 43, 326, 19, 249);
INSERT INTO `roads` VALUES (26, 3, 12, 6, 12, 33, 328, 78, 477);
INSERT INTO `roads` VALUES (27, 3, 12, 12, 6, 88, 475, 43, 326);
INSERT INTO `roads` VALUES (28, 2, 7, 2, 7, 141, 203, 163, 282);
INSERT INTO `roads` VALUES (29, 2, 7, 7, 2, 173, 280, 151, 201);
INSERT INTO `roads` VALUES (30, 2, 8, 3, 8, 263, 155, 288, 238);
INSERT INTO `roads` VALUES (31, 2, 8, 8, 3, 298, 236, 273, 153);
INSERT INTO `roads` VALUES (32, 3, 13, 8, 13, 288, 238, 324, 380);
INSERT INTO `roads` VALUES (33, 3, 13, 13, 8, 334, 378, 298, 236);
INSERT INTO `roads` VALUES (34, 2, 9, 4, 9, 364, 114, 394, 204);
INSERT INTO `roads` VALUES (35, 2, 9, 9, 4, 404, 200, 374, 110);
INSERT INTO `roads` VALUES (36, 3, 14, 14, 25, 427, 343, 483, 521);
INSERT INTO `roads` VALUES (37, 3, 14, 25, 14, 493, 517, 437, 339);
INSERT INTO `roads` VALUES (38, 3, 15, 10, 15, 522, 160, 553, 291);
INSERT INTO `roads` VALUES (39, 2, 11, 5, 11, 545, 5, 601, 129);
INSERT INTO `roads` VALUES (40, 2, 11, 11, 5, 611, 125, 555, 1);
INSERT INTO `roads` VALUES (41, 3, 16, 11, 16, 601, 128, 638, 257);
INSERT INTO `roads` VALUES (42, 3, 16, 16, 11, 648, 255, 611, 126);
INSERT INTO `roads` VALUES (43, 1, 1, 17, 1, 1, 216, 9, 251);
INSERT INTO `roads` VALUES (44, 1, 1, 1, 17, 19, 249, 11, 214);
INSERT INTO `roads` VALUES (45, 1, 3, 18, 3, 243, 90, 263, 155);
INSERT INTO `roads` VALUES (46, 1, 3, 3, 18, 273, 153, 253, 88);
INSERT INTO `roads` VALUES (47, 1, 4, 19, 4, 340, 40, 364, 114);
INSERT INTO `roads` VALUES (48, 1, 4, 4, 19, 374, 110, 350, 36);
INSERT INTO `roads` VALUES (49, 1, 1, 20, 1, 1, 250, 12, 245);
INSERT INTO `roads` VALUES (50, 1, 1, 1, 20, 16, 255, 5, 260);
INSERT INTO `roads` VALUES (51, 2, 6, 21, 6, 1, 336, 36, 322);
INSERT INTO `roads` VALUES (52, 2, 6, 6, 21, 40, 332, 5, 346);
INSERT INTO `roads` VALUES (53, 2, 11, 11, 22, 603, 123, 638, 93);
INSERT INTO `roads` VALUES (54, 2, 11, 22, 11, 644, 101, 609, 131);
INSERT INTO `roads` VALUES (55, 3, 12, 23, 12, 3, 502, 83, 476);
INSERT INTO `roads` VALUES (56, 3, 12, 12, 24, 78, 478, 93, 522);
INSERT INTO `roads` VALUES (57, 3, 12, 24, 12, 103, 518, 88, 474);

-- ----------------------------
-- Table structure for threshold
-- ----------------------------
DROP TABLE IF EXISTS `threshold`;
CREATE TABLE `threshold`  (
  `id` int NULL DEFAULT NULL,
  `road_id` int NULL DEFAULT NULL,
  `G1` int NULL DEFAULT NULL,
  `G2` int NULL DEFAULT NULL,
  `G3` int NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of threshold
-- ----------------------------
INSERT INTO `threshold` VALUES (1, 1, 30, 40, 50);
INSERT INTO `threshold` VALUES (2, 2, 30, 40, 50);
INSERT INTO `threshold` VALUES (3, 3, 30, 40, 50);
INSERT INTO `threshold` VALUES (4, 4, 30, 40, 50);
INSERT INTO `threshold` VALUES (5, 5, 30, 40, 50);
INSERT INTO `threshold` VALUES (6, 6, 30, 40, 50);
INSERT INTO `threshold` VALUES (7, 7, 30, 40, 50);
INSERT INTO `threshold` VALUES (8, 8, 30, 40, 50);
INSERT INTO `threshold` VALUES (9, 9, 140, 150, 160);
INSERT INTO `threshold` VALUES (10, 10, 300, 320, 330);
INSERT INTO `threshold` VALUES (11, 11, 260, 290, 300);
INSERT INTO `threshold` VALUES (12, 12, 300, 320, 330);
INSERT INTO `threshold` VALUES (13, 13, 200, 220, 230);
INSERT INTO `threshold` VALUES (14, 14, 150, 170, 180);
INSERT INTO `threshold` VALUES (15, 15, 200, 220, 230);
INSERT INTO `threshold` VALUES (16, 16, 150, 170, 180);
INSERT INTO `threshold` VALUES (17, 17, 200, 220, 230);
INSERT INTO `threshold` VALUES (18, 18, 190, 220, 230);
INSERT INTO `threshold` VALUES (19, 19, 180, 220, 230);
INSERT INTO `threshold` VALUES (20, 20, 170, 190, 200);
INSERT INTO `threshold` VALUES (21, 21, 180, 200, 210);
INSERT INTO `threshold` VALUES (22, 22, 30, 40, 50);
INSERT INTO `threshold` VALUES (23, 23, 90, 120, 130);
INSERT INTO `threshold` VALUES (24, 24, 110, 130, 140);
INSERT INTO `threshold` VALUES (25, 25, 130, 150, 160);
INSERT INTO `threshold` VALUES (26, 26, 40, 50, 60);
INSERT INTO `threshold` VALUES (27, 27, 40, 50, 60);
INSERT INTO `threshold` VALUES (28, 28, 30, 40, 50);
INSERT INTO `threshold` VALUES (29, 29, 30, 40, 50);
INSERT INTO `threshold` VALUES (30, 30, 60, 70, 80);
INSERT INTO `threshold` VALUES (31, 31, 60, 70, 80);
INSERT INTO `threshold` VALUES (32, 32, 140, 160, 170);
INSERT INTO `threshold` VALUES (33, 33, 140, 160, 170);
INSERT INTO `threshold` VALUES (34, 34, 30, 40, 50);
INSERT INTO `threshold` VALUES (35, 35, 60, 70, 80);
INSERT INTO `threshold` VALUES (36, 36, 40, 50, 60);
INSERT INTO `threshold` VALUES (37, 37, 35, 40, 50);
INSERT INTO `threshold` VALUES (38, 38, 80, 100, 110);
INSERT INTO `threshold` VALUES (39, 39, 230, 240, 250);
INSERT INTO `threshold` VALUES (40, 40, 300, 320, 330);
INSERT INTO `threshold` VALUES (41, 41, 160, 170, 180);
INSERT INTO `threshold` VALUES (42, 42, 140, 160, 170);
INSERT INTO `threshold` VALUES (43, 43, 160, 170, 180);
INSERT INTO `threshold` VALUES (44, 44, 110, 120, 130);
INSERT INTO `threshold` VALUES (45, 45, 60, 70, 80);
INSERT INTO `threshold` VALUES (46, 46, 60, 70, 80);
INSERT INTO `threshold` VALUES (47, 47, 30, 40, 50);
INSERT INTO `threshold` VALUES (48, 48, 60, 70, 80);
INSERT INTO `threshold` VALUES (49, 49, 30, 40, 50);
INSERT INTO `threshold` VALUES (50, 50, 30, 40, 50);
INSERT INTO `threshold` VALUES (51, 51, 200, 220, 230);
INSERT INTO `threshold` VALUES (52, 52, 120, 145, 150);
INSERT INTO `threshold` VALUES (53, 53, 200, 220, 230);
INSERT INTO `threshold` VALUES (54, 54, 200, 220, 230);
INSERT INTO `threshold` VALUES (55, 55, 180, 200, 210);
INSERT INTO `threshold` VALUES (56, 56, 25, 40, 50);
INSERT INTO `threshold` VALUES (57, 57, 25, 40, 50);

-- ----------------------------
-- Table structure for time
-- ----------------------------
DROP TABLE IF EXISTS `time`;
CREATE TABLE `time`  (
  `id` int NULL DEFAULT NULL,
  `time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of time
-- ----------------------------
INSERT INTO `time` VALUES (1, '00:00:00');
INSERT INTO `time` VALUES (2, '00:10:00');
INSERT INTO `time` VALUES (3, '00:20:00');
INSERT INTO `time` VALUES (4, '00:30:00');
INSERT INTO `time` VALUES (5, '00:40:00');
INSERT INTO `time` VALUES (6, '00:50:00');
INSERT INTO `time` VALUES (7, '01:00:00');
INSERT INTO `time` VALUES (8, '01:10:00');
INSERT INTO `time` VALUES (9, '01:20:00');
INSERT INTO `time` VALUES (10, '01:30:00');
INSERT INTO `time` VALUES (11, '01:40:00');
INSERT INTO `time` VALUES (12, '01:50:00');
INSERT INTO `time` VALUES (13, '02:00:00');
INSERT INTO `time` VALUES (14, '02:10:00');
INSERT INTO `time` VALUES (15, '02:20:00');
INSERT INTO `time` VALUES (16, '02:30:00');
INSERT INTO `time` VALUES (17, '02:40:00');
INSERT INTO `time` VALUES (18, '02:50:00');
INSERT INTO `time` VALUES (19, '03:00:00');
INSERT INTO `time` VALUES (20, '03:10:00');
INSERT INTO `time` VALUES (21, '03:20:00');
INSERT INTO `time` VALUES (22, '03:30:00');
INSERT INTO `time` VALUES (23, '03:40:00');
INSERT INTO `time` VALUES (24, '03:50:00');
INSERT INTO `time` VALUES (25, '04:00:00');
INSERT INTO `time` VALUES (26, '04:10:00');
INSERT INTO `time` VALUES (27, '04:20:00');
INSERT INTO `time` VALUES (28, '04:30:00');
INSERT INTO `time` VALUES (29, '04:40:00');
INSERT INTO `time` VALUES (30, '04:50:00');
INSERT INTO `time` VALUES (31, '05:00:00');
INSERT INTO `time` VALUES (32, '05:10:00');
INSERT INTO `time` VALUES (33, '05:20:00');
INSERT INTO `time` VALUES (34, '05:30:00');
INSERT INTO `time` VALUES (35, '05:40:00');
INSERT INTO `time` VALUES (36, '05:50:00');
INSERT INTO `time` VALUES (37, '06:00:00');
INSERT INTO `time` VALUES (38, '06:10:00');
INSERT INTO `time` VALUES (39, '06:20:00');
INSERT INTO `time` VALUES (40, '06:30:00');
INSERT INTO `time` VALUES (41, '06:40:00');
INSERT INTO `time` VALUES (42, '06:50:00');
INSERT INTO `time` VALUES (43, '07:00:00');
INSERT INTO `time` VALUES (44, '07:10:00');
INSERT INTO `time` VALUES (45, '07:20:00');
INSERT INTO `time` VALUES (46, '07:30:00');
INSERT INTO `time` VALUES (47, '07:40:00');
INSERT INTO `time` VALUES (48, '07:50:00');
INSERT INTO `time` VALUES (49, '08:00:00');
INSERT INTO `time` VALUES (50, '08:10:00');
INSERT INTO `time` VALUES (51, '08:20:00');
INSERT INTO `time` VALUES (52, '08:30:00');
INSERT INTO `time` VALUES (53, '08:40:00');
INSERT INTO `time` VALUES (54, '08:50:00');
INSERT INTO `time` VALUES (55, '09:00:00');
INSERT INTO `time` VALUES (56, '09:10:00');
INSERT INTO `time` VALUES (57, '09:20:00');
INSERT INTO `time` VALUES (58, '09:30:00');
INSERT INTO `time` VALUES (59, '09:40:00');
INSERT INTO `time` VALUES (60, '09:50:00');
INSERT INTO `time` VALUES (61, '10:00:00');
INSERT INTO `time` VALUES (62, '10:10:00');
INSERT INTO `time` VALUES (63, '10:20:00');
INSERT INTO `time` VALUES (64, '10:30:00');
INSERT INTO `time` VALUES (65, '10:40:00');
INSERT INTO `time` VALUES (66, '10:50:00');
INSERT INTO `time` VALUES (67, '11:00:00');
INSERT INTO `time` VALUES (68, '11:10:00');
INSERT INTO `time` VALUES (69, '11:20:00');
INSERT INTO `time` VALUES (70, '11:30:00');
INSERT INTO `time` VALUES (71, '11:40:00');
INSERT INTO `time` VALUES (72, '11:50:00');
INSERT INTO `time` VALUES (73, '12:00:00');
INSERT INTO `time` VALUES (74, '12:10:00');
INSERT INTO `time` VALUES (75, '12:20:00');
INSERT INTO `time` VALUES (76, '12:30:00');
INSERT INTO `time` VALUES (77, '12:40:00');
INSERT INTO `time` VALUES (78, '12:50:00');
INSERT INTO `time` VALUES (79, '13:00:00');
INSERT INTO `time` VALUES (80, '13:10:00');
INSERT INTO `time` VALUES (81, '13:20:00');
INSERT INTO `time` VALUES (82, '13:30:00');
INSERT INTO `time` VALUES (83, '13:40:00');
INSERT INTO `time` VALUES (84, '13:50:00');
INSERT INTO `time` VALUES (85, '14:00:00');
INSERT INTO `time` VALUES (86, '14:10:00');
INSERT INTO `time` VALUES (87, '14:20:00');
INSERT INTO `time` VALUES (88, '14:30:00');
INSERT INTO `time` VALUES (89, '14:40:00');
INSERT INTO `time` VALUES (90, '14:50:00');
INSERT INTO `time` VALUES (91, '15:00:00');
INSERT INTO `time` VALUES (92, '15:10:00');
INSERT INTO `time` VALUES (93, '15:20:00');
INSERT INTO `time` VALUES (94, '15:30:00');
INSERT INTO `time` VALUES (95, '15:40:00');
INSERT INTO `time` VALUES (96, '15:50:00');
INSERT INTO `time` VALUES (97, '16:00:00');
INSERT INTO `time` VALUES (98, '16:10:00');
INSERT INTO `time` VALUES (99, '16:20:00');
INSERT INTO `time` VALUES (100, '16:30:00');
INSERT INTO `time` VALUES (101, '16:40:00');
INSERT INTO `time` VALUES (102, '16:50:00');
INSERT INTO `time` VALUES (103, '17:00:00');
INSERT INTO `time` VALUES (104, '17:10:00');
INSERT INTO `time` VALUES (105, '17:20:00');
INSERT INTO `time` VALUES (106, '17:30:00');
INSERT INTO `time` VALUES (107, '17:40:00');
INSERT INTO `time` VALUES (108, '17:50:00');
INSERT INTO `time` VALUES (109, '18:00:00');
INSERT INTO `time` VALUES (110, '18:10:00');
INSERT INTO `time` VALUES (111, '18:20:00');
INSERT INTO `time` VALUES (112, '18:30:00');
INSERT INTO `time` VALUES (113, '18:40:00');
INSERT INTO `time` VALUES (114, '18:50:00');
INSERT INTO `time` VALUES (115, '19:00:00');
INSERT INTO `time` VALUES (116, '19:10:00');
INSERT INTO `time` VALUES (117, '19:20:00');
INSERT INTO `time` VALUES (118, '19:30:00');
INSERT INTO `time` VALUES (119, '19:40:00');
INSERT INTO `time` VALUES (120, '19:50:00');
INSERT INTO `time` VALUES (121, '20:00:00');
INSERT INTO `time` VALUES (122, '20:10:00');
INSERT INTO `time` VALUES (123, '20:20:00');
INSERT INTO `time` VALUES (124, '20:30:00');
INSERT INTO `time` VALUES (125, '20:40:00');
INSERT INTO `time` VALUES (126, '20:50:00');
INSERT INTO `time` VALUES (127, '21:00:00');
INSERT INTO `time` VALUES (128, '21:10:00');
INSERT INTO `time` VALUES (129, '21:20:00');
INSERT INTO `time` VALUES (130, '21:30:00');
INSERT INTO `time` VALUES (131, '21:40:00');
INSERT INTO `time` VALUES (132, '21:50:00');
INSERT INTO `time` VALUES (133, '22:00:00');
INSERT INTO `time` VALUES (134, '22:10:00');
INSERT INTO `time` VALUES (135, '22:20:00');
INSERT INTO `time` VALUES (136, '22:30:00');
INSERT INTO `time` VALUES (137, '22:40:00');
INSERT INTO `time` VALUES (138, '22:50:00');
INSERT INTO `time` VALUES (139, '23:00:00');
INSERT INTO `time` VALUES (140, '23:10:00');
INSERT INTO `time` VALUES (141, '23:20:00');
INSERT INTO `time` VALUES (142, '23:30:00');
INSERT INTO `time` VALUES (143, '23:40:00');
INSERT INTO `time` VALUES (144, '23:50:00');

SET FOREIGN_KEY_CHECKS = 1;
