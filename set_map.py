from PyQt5.QtCore import pyqtSignal
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtWidgets import QMainWindow, QColorDialog, QMessageBox
from PyQt5.uic import loadUiType

ui, _ = loadUiType("./resource/ui/map.ui")


# 配置地图界面
class Setup_Map(QMainWindow, ui):
    mapInfoEmit = pyqtSignal(list)

    def __init__(self):
        QMainWindow.__init__(self)
        self.setupUi(self)
        self.setWindowIcon(QIcon("./resource/icon/setup.png"))
        self.setWindowTitle("地图配置")
        self.setFixedSize(self.width(), self.height())
        # 默认设置数值
        self.map_road_width = 3
        self.map_crowded_level_color = []
        # 拥堵颜色设置
        self.color_0 = [0, 255, 0]    # 绿色
        self.color_1 = [255, 255, 0]  # 黄色
        self.color_2 = [255, 0, 0]    # 红色
        self.color_3 = [165, 42, 42]  # 褐色
        # 设置道路宽度范围
        self.SpinBox_width.setRange(1, 10)
        self.SpinBox_width.setSingleStep(1)
        # 设置默认道路宽度
        self.SpinBox_width.setValue(self.map_road_width)
        # 按钮图标
        self.pushButton_default.setIcon(QIcon(QPixmap("./resource/icon/default.png")))
        self.pushButton_setup.setIcon(QIcon(QPixmap("./resource/icon/set.png")))
        # 槽函数连接
        self.pushButton.clicked.connect(lambda: self.set_color(self.pushButton))
        self.pushButton_1.clicked.connect(lambda: self.set_color(self.pushButton_1))
        self.pushButton_2.clicked.connect(lambda: self.set_color(self.pushButton_2))
        self.pushButton_3.clicked.connect(lambda: self.set_color(self.pushButton_3))
        self.pushButton_default.clicked.connect(self.setup_default_map)
        self.pushButton_setup.clicked.connect(self.setup_map)

    # 加载qss样式
    @staticmethod
    def load_qss(button):
        col = QColorDialog.getColor()
        r, g, b = col.red(), col.green(), col.blue()
        qss = 'QWidget {background-color:rgb(%s, %s, %s)}' % (r, g, b)
        button.setStyleSheet(qss)
        return r, g, b

    # 设置颜色变量
    def set_color(self, button):
        color = [self.load_qss(button)]
        if button == self.pushButton:
            self.color_0 = color
        elif button == self.pushButton_1:
            self.color_1 = color
        elif button == self.pushButton_2:
            self.color_2 = color
        else:
            self.color_3 = color

    # 默认地图配置
    def setup_default_map(self):
        QMessageBox.about(self, "提示", "加载默认地图设置")
        # 设置默认道路宽度
        self.SpinBox_width.setValue(3)
        # 设置默认拥堵颜色
        self.pushButton.setStyleSheet('QWidget {background-color:rgb(0, 255, 0)}')
        self.pushButton_1.setStyleSheet('QWidget {background-color:rgb(255, 255, 0)}')
        self.pushButton_2.setStyleSheet('QWidget {background-color:rgb(255, 0, 0)}')
        self.pushButton_3.setStyleSheet('QWidget {background-color:rgb(165, 42, 42)}')
        # 拥堵颜色设置
        self.color_0 = [0, 255, 0]    # 绿色
        self.color_1 = [255, 255, 0]  # 黄色
        self.color_2 = [255, 0, 0]    # 红色
        self.color_3 = [165, 42, 42]  # 褐色

    # 完成地图配置
    def setup_map(self):
        self.map_road_width = self.SpinBox_width.value()
        self.map_crowded_level_color = [self.color_0, self.color_1, self.color_2, self.color_3]
        self.mapInfoEmit.emit([self.map_road_width, self.map_crowded_level_color])
        QMessageBox.about(self, '提示', '地图配置成功')
