from PyQt5.QtCore import Qt
from PyQt5.QtGui import QMovie
from PyQt5.QtWidgets import QMainWindow
from PyQt5.uic import loadUiType

ui, _ = loadUiType("./resource/ui/waiting.ui")


# 等待连接界面
class wait_connect(QMainWindow, ui):
    def __init__(self):
        QMainWindow.__init__(self)
        self.setupUi(self)
        self.setWindowOpacity(0.7)
        self.setWindowFlags(Qt.Dialog | Qt.FramelessWindowHint)
        self.Move = QMovie("./resource/icon/load_connect.gif")
        self.label.setMovie(self.Move)
        self.Move.start()
