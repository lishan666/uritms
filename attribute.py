from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QDialog
from PyQt5.uic import loadUiType

ui, _ = loadUiType("./resource/ui/attribute.ui")


# 配置选点信息界面
class point_attribute(QDialog, ui):
    attribute = 0, 0, 0, 0

    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.setWindowIcon(QIcon("./resource/icon/about.png"))
        self.setWindowTitle("选点属性")
        self.setFixedSize(self.width(), self.height())
        self.setWindowFlags(Qt.Dialog | Qt.WindowStaysOnTopHint)

    def get_attribute(self):
        if self.radioButton_cross.isChecked():
            is_cross = 1
        else:
            is_cross = 0
        area_num = self.spinBox_1.value()
        cross_num = self.spinBox_2.value()
        phase_amount = self.spinBox_3.value()
        return is_cross, area_num, cross_num, phase_amount

    def accept(self) -> None:
        self.attribute = self.get_attribute()
        return QDialog.accept(self)

    def reject(self) -> None:
        return QDialog.reject(self)
