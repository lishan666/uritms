本项目快速安装
【Windows 10】
方法1：pip install -r requirements.txt -i https://pypi.douban.com/simple/
方法2：运行install_module.bat

【Linux(Ubuntu 20.04)】
方法1：
pip3 install -r requirements.txt -i https://pypi.douban.com/simple/
pip3 uninstall opencv-python
pip3 install opencv-contrib-python
sudo cd ~
sudo cd .local/lib/python3.8/site-packages/
sudo cp -r PyQt5/Qt/plugins/platforms/* cv2/qt/plugins/platforms/
sudo rm /lib/x86_64-linux-gnu/libQt5XcbQpa*
sudo apt-get install python3-opencv
sudo apt-get install xchm
方法2：终端运行 sudo sh install_module.sh

本项目版本要求：
（1）操作系统  ：Windows 10
（2）开发工具  ：PyCharm Professional 2020.3.3
（3）开发语言  ：Python 3.8.3
（4）界面设计  ：Qt Designer 5.15.1
（5）数据库    ：MySQL 8.0.23
（6）数据库管理 ：Navicat Premium 15

Python导入的主要模块有：
    GUI开发组件核心：PyQt5==5.15.2
    MySQL数据库操作：PyMySQL==1.0.2
    实时视频帧处理：opencv-python==4.5.1.48
    Excle表格文件读写：pandas==1.2.3
    科学计算库：sympy==1.7.1
    表格类数据美化：prettytable==2.0.0
    Exe可执行文件打包：pyinstaller==4.2

部署项目环境方法
python项目中最好包含一个 requirements.txt 文件，用于记录所有依赖包及其精确的版本号。以便新环境部署。
requirements.txt可以通过pip命令自动生成和安装

生成requirements.txt文件命令
pip freeze > requirements.txt

安装requirements.txt依赖命令
pip install -r requirements.txt

更换国内镜像，可加快安装速度

豆瓣
pip install -r requirements.txt -i https://pypi.douban.com/simple/
阿里云
pip install -r requirements.txt -i https://mirrors.aliyun.com/pypi/simple/
清华
pip install -r requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple/


PyQt 打包exe方法
【安装打包工具】
pip install -i https://pypi.tuna.tsinghua.edu.cn/simple pyinstaller

【打包命令】
pyinstaller -Fwi icon.ico main.py

*参数说明*
-F, –onefile 打包成一个exe文件。
-D, –onedir 创建一个目录，包含exe文件，但会依赖很多文件（默认选项）。
-c, –console, –nowindowed 使用控制台，无界面(默认)
-w, –windowed, –noconsole 使用窗口，无控制台
-i, 设置应用图标
icon.ico  应用图标路径(获取icon图标参考网址：https://www.easyicon.net/)
main.py   py主程序路径
