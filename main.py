import sys
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox, QDesktopWidget
from PyQt5.uic import loadUiType
import interface

ui, _ = loadUiType("./resource/ui/login.ui")


# 登录界面
class LogIn(QMainWindow, ui):
    def __init__(self):
        QMainWindow.__init__(self)
        self.setupUi(self)
        self.setWindowIcon(QIcon("./resource/icon/icon.png"))
        self.setFixedSize(self.width(), self.height())
        self.user = "admin"
        self.password = "lucky123"
        self.btnLogin.clicked.connect(self.login_clicked)
        self.btnClear.clicked.connect(self.clear_clicked)

    def login_clicked(self):
        verify_result = self.txtUserID.text() == self.user and self.txtPWD.text() == self.password
        if verify_result:
            screen = QDesktopWidget().screenGeometry()
            size = window.geometry()
            x = (screen.width() - size.width()) / 2
            y = (screen.height() - size.height()) / 2 - (20 / 768 * screen.height())
            window.move(int(x), int(y))
            window.show()
            self.close()
        else:
            self.clear_clicked()
            QMessageBox.warning(self, '警告', '登录失败，请检查账号或密码是否输入正确')

    def clear_clicked(self):
        self.txtUserID.setText("")
        self.txtPWD.setText("")


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = interface.MainWindow()
    login = LogIn()
    login.show()
    sys.exit(app.exec_())
